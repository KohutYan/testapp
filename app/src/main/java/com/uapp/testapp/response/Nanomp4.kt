package com.uapp.testapp.response


import com.google.gson.annotations.SerializedName

data class Nanomp4(
    val dims: List<Int>,
    val duration: Double,
    val preview: String,
    val size: Int,
    val url: String
)