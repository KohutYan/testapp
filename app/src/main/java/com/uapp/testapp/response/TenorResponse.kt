package com.uapp.testapp.response


import com.google.gson.annotations.SerializedName

data class TenorResponse(
    val next: String,
    val results: List<Result>,
    val weburl: String
)