package com.uapp.testapp.response


import androidx.room.*
import com.google.gson.annotations.SerializedName
data class Result(
    val composite: Any,
    val created: Double,
    val hasaudio: Boolean,
    val id: String = "id",
    val itemurl: String = "itemurl",
    //@Embedded(prefix = "media_")
    val media: List<Media>? = listOf<Media>(),
    val shares: Int,
    val tags: List<Any>,
    val title: String? = "title",
    val url: String = "url"
)