package com.uapp.testapp.response


import com.google.gson.annotations.SerializedName

data class Nanogif(
    val dims: List<Int>,
    val preview: String,
    val size: Int,
    val url: String
)