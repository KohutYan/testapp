package com.uapp.testapp.db

import androidx.lifecycle.LiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.uapp.testapp.CachedResult
import com.uapp.testapp.response.Result
import com.uapp.testapp.retrofit.TenorPagingSource
import kotlinx.coroutines.flow.Flow

class ResultRepo(private val cachedResultDao: CachedResultDao) {
    val allResults: LiveData<MutableList<CachedResult>> = cachedResultDao.getResultsFromDao()

    suspend fun insert(result: CachedResult) {
        cachedResultDao.insert(result)
    }

    suspend fun delete(id: String){
        cachedResultDao.deleteById(id)
    }

    fun getSearchResultStream(query: String): Flow<PagingData<Result>> {
        return Pager(
            config = PagingConfig(
                pageSize = PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {TenorPagingSource(query)}).flow
    }
    companion object{
        private const val PAGE_SIZE = 10
    }

}