package com.uapp.testapp.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.uapp.testapp.CachedResult

@Database(
    entities = [CachedResult::class],
    version = 1,
    exportSchema = false
)

abstract class MainDB : RoomDatabase() {

    abstract fun resultDao(): CachedResultDao

    companion object {
        @Volatile
        private var INSTANCE: MainDB? = null

        fun getDB(context: Context): MainDB {
            val tempInstance = INSTANCE
            if (INSTANCE != null) {
                return tempInstance!!
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MainDB::class.java,
                    "main_db"
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                return instance
            }
        }
    }
}