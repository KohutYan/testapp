package com.uapp.testapp.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.uapp.testapp.CachedResult

@Dao
interface CachedResultDao {
    @Query("SELECT * from cached_results")
    fun getResultsFromDao(): LiveData<MutableList<CachedResult>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(cachedResult: CachedResult)

    @Query("DELETE FROM cached_results")
    suspend fun clear()

    @Query("DELETE FROM cached_results WHERE id = :id")
    suspend fun deleteById(id: String)
}