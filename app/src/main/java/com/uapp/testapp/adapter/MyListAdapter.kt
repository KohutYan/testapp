package com.uapp.testapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.uapp.testapp.CachedResult
import com.uapp.testapp.R
import kotlinx.android.synthetic.main.gif_item.view.*
import java.io.File

@Suppress("DEPRECATION")
class MyListAdapter(val items: MutableList<CachedResult>, val context: Context) : RecyclerView.Adapter<MyListAdapter.ViewHolder>(){

    var onItemClick: ((CachedResult) -> Unit)? = null
    var onItemHold: ((CachedResult) -> Unit)? = null
    var onCheckedChange: ((CachedResult) -> Unit)? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.gif_item, parent, false)
        return ViewHolder(cellForRow)
    }

    fun setList(results: MutableList<CachedResult>?){
        if (results != null) {
            items.clear()
            items.addAll(results)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val gifItem = items[position]
        val img = holder.gifImg
        if (gifItem.title!!.isEmpty()) {
            holder.gifName.text = "*no name*"
        } else {
            holder.gifName.text = gifItem.title
        }
        Glide.with(holder.itemView).asGif().load(gifItem.nanoUrl).into(img)
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val gifImg: ImageView = itemView.gifIV
        val gifName: TextView = itemView.gifNameTV
        val checkBox: CheckBox = itemView.favCheckBox

        init {
            checkBox.isChecked = true
            itemView.setOnClickListener {
                onItemClick?.invoke(items[adapterPosition])
            }
            itemView.favCheckBox.setOnCheckedChangeListener { buttonView, _ ->
                buttonView.isChecked = false
                 onCheckedChange?.invoke(items[adapterPosition])
                notifyItemRemoved(adapterPosition)
            }
            itemView.setOnLongClickListener {
                download(items[adapterPosition], itemView)
                onItemHold?.invoke(items[adapterPosition])
                true
            }
        }
    }

    fun download(result: CachedResult, itemView: View){
        Glide.with(itemView)
            .download(result.url)
            .listener(object : RequestListener<File>{
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<File>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: File?,
                    model: Any?,
                    target: Target<File>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }
            }).submit()

    }
}
