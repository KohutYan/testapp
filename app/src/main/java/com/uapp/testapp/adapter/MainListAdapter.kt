package com.uapp.testapp.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.uapp.testapp.R
import com.uapp.testapp.response.Result
import kotlinx.android.synthetic.main.gif_item.view.*
import java.io.File

@Suppress("DEPRECATION")
class MainListAdapter : PagingDataAdapter<Result, MainListAdapter.ViewHolder>(COMPARATOR) {

    var onItemClick: ((Result) -> Unit)? = null
    var onItemHold: ((Result) -> Unit)? = null
    var onCheckedChangeTrue: ((Result) -> Unit)? = null
    var onCheckedChangeFalse: ((Result) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForItem = layoutInflater.inflate(R.layout.gif_item, parent, false)

        return ViewHolder(cellForItem)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val gifItem = getItem(position)
        val img = holder.gifImg
        if (gifItem?.title!!.isEmpty()) {
            holder.gifName.text = "*no name*"
        } else {
            holder.gifName.text = gifItem.title
        }
        Glide.with(holder.itemView).asGif().load(gifItem.media!![0].nanogif.url).into(img)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val gifImg: ImageView = itemView.gifIV
        val gifName: TextView = itemView.gifNameTV

        init {
            itemView.setOnClickListener {
                getItem(adapterPosition)?.let { it1 -> onItemClick?.invoke(it1) }
            }
            itemView.favCheckBox.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    buttonView.isChecked = true
                    getItem(adapterPosition)?.let { it2 -> onCheckedChangeTrue?.invoke(it2) }
                } else {
                    buttonView.isChecked = false
                    getItem(adapterPosition)?.let { it2 -> onCheckedChangeFalse?.invoke(it2) }
                }
            }
            itemView.setOnLongClickListener {
                download(getItem(adapterPosition), itemView)
                getItem(adapterPosition)?.let { it1 -> onItemHold?.invoke(it1) }
                true
            }
        }
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<Result>() {
            override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean =
                newItem == oldItem
        }
    }

    fun download(result: Result?, itemView: View) {
        Glide.with(itemView)
            .download(result?.url)
            .listener(object : RequestListener<File> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<File>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: File?,
                    model: Any?,
                    target: Target<File>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }
            }).submit()

    }
}