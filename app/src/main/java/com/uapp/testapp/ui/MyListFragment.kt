@file:Suppress("DEPRECATION")

package com.uapp.testapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.uapp.testapp.R
import com.uapp.testapp.adapter.MyListAdapter
import com.uapp.testapp.viewmodel.MyListViewModel
import kotlinx.android.synthetic.main.my_list_fragment.view.*

@Suppress("DEPRECATION")
class MyListFragment : Fragment() {

    companion object {
        fun newInstance() = MyListFragment()
    }

    private lateinit var viewModel: MyListViewModel
    private lateinit var adapter: MyListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        adapter = MyListAdapter(mutableListOf(), activity!!.applicationContext)
        val rootView = inflater.inflate(R.layout.my_list_fragment, container, false)
        val mainList = rootView.myListRV as RecyclerView
        mainList.adapter = adapter
        mainList.layoutManager = LinearLayoutManager(activity)

        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MyListViewModel::class.java)

        viewModel.cachedResults.observe(viewLifecycleOwner, Observer {
            adapter.setList(it)
        })


        adapter.onItemClick = { result ->
            val newFragment = FullscreenGifFragment()
            val args = Bundle()
            args.putString("gif_url", result.url)
            newFragment.arguments = args

            fragmentManager?.beginTransaction()?.replace(R.id.my_list, newFragment)?.commit()
        }
        adapter.onCheckedChange = {result ->
            viewModel.delete(result)
        }
        adapter.onItemHold = {
            Toast.makeText(activity!!.applicationContext, getString(R.string.download), Toast.LENGTH_SHORT).show()
        }
    }

}