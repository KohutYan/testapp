package com.uapp.testapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.uapp.testapp.R
import kotlinx.android.synthetic.main.fragment_fullscreen_gif.view.*

class FullscreenGifFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var imageView: ImageView
    private var gifUrl: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            gifUrl = arguments?.getString("gif_url")!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_fullscreen_gif, container, false)
        imageView = rootView.imageView as ImageView
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Glide.with(this).asGif().load(gifUrl).into(imageView)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.isClickable = true
        view.isFocusableInTouchMode = true
        view.isFocusable = true
    }
}