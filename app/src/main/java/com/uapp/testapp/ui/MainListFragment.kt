@file:Suppress("DEPRECATION")

package com.uapp.testapp.ui

import android.content.Context.INPUT_METHOD_SERVICE
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.uapp.testapp.viewmodel.MainListViewModel
import com.uapp.testapp.R
import com.uapp.testapp.adapter.MainListAdapter
import kotlinx.android.synthetic.main.main_list_fragment.*
import kotlinx.android.synthetic.main.main_list_fragment.view.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collectLatest

@Suppress("DEPRECATION")
class MainListFragment : Fragment() {

    companion object {
        fun newInstance() = MainListFragment()
    }

    private lateinit var viewModel: MainListViewModel
    private lateinit var adapter: MainListAdapter

    private var searchJob: Job? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        adapter = MainListAdapter()
        val rootView = inflater.inflate(R.layout.main_list_fragment, container, false)
        val mainList = rootView.maiListRV as RecyclerView
        mainList.adapter = adapter
        mainList.layoutManager = LinearLayoutManager(activity)

        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainListViewModel::class.java)

        searchButton.setOnClickListener {
            val searchQuery = searchET.text.toString()
            search(searchQuery)
        }

        adapter.onItemClick = { result ->
            val newFragment = FullscreenGifFragment()
            val args = Bundle()
            args.putString("gif_url", result.media!![0].gif.url)
            newFragment.arguments = args

            //searchET.imeOptions = EditorInfo.IME_ACTION_DONE
            view?.let { hideSoftKeyboard(it) }

            fragmentManager?.beginTransaction()
                ?.replace(R.id.main_list, newFragment)
                ?.setTransition(
                    FragmentTransaction.TRANSIT_FRAGMENT_FADE
                )
                ?.addToBackStack(null)?.commit()
        }

        adapter.onItemHold = {
            Toast.makeText(activity!!.applicationContext, getString(R.string.download), Toast.LENGTH_SHORT).show()
        }

        adapter.onCheckedChangeTrue = { result ->
            viewModel.insert(result)
        }
        adapter.onCheckedChangeFalse = { result ->
            viewModel.delete(result)
        }
    }

    private fun hideSoftKeyboard(view: View) {
        val imm =
            view.context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun search(query: String) {
        searchJob?.cancel()
        searchJob = lifecycleScope.launch {
            viewModel.searchByQuery(query).collectLatest {
                adapter.submitData(it)
            }
        }
    }
}