package com.uapp.testapp

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cached_results")
data class CachedResult(
    @ColumnInfo(name = "url")
    val url: String,
    @ColumnInfo(name = "nano_url")
    val nanoUrl: String,
    @ColumnInfo(name = "title")
    val title: String?,
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    val id: String
)