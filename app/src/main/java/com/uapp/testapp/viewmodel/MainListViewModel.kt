package com.uapp.testapp.viewmodel

import android.app.Application
import androidx.lifecycle.*
import androidx.paging.*
import com.uapp.testapp.CachedResult
import com.uapp.testapp.db.ResultRepo
import com.uapp.testapp.db.MainDB
import com.uapp.testapp.response.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class MainListViewModel(application: Application) : AndroidViewModel(application) {

    private val dao = MainDB.getDB(application).resultDao()
    private val repo = ResultRepo(dao)

    private var currentQueryValue: String? = null
    private var currentSearchResult: Flow<PagingData<Result>>? = null

    fun searchByQuery(query: String): Flow<PagingData<Result>> {
        val lastResult = currentSearchResult
        if (query == currentQueryValue && lastResult != null) {
            return lastResult
        }
        currentQueryValue = query
        val newResult: Flow<PagingData<Result>> =
            repo.getSearchResultStream(query).cachedIn(viewModelScope)
        currentSearchResult = newResult

        return newResult
    }

    fun insert(result: Result) = viewModelScope.launch {
        val cachedResult = CachedResult(result.media!![0].gif.url, result.media!![0].nanogif.url,
            result.title!!, result.id)
        repo.insert(cachedResult)
    }

    fun delete(result: Result) = viewModelScope.launch{
        repo.delete(result.id)
    }

}
