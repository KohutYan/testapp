package com.uapp.testapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.uapp.testapp.CachedResult
import com.uapp.testapp.db.MainDB
import com.uapp.testapp.db.ResultRepo
import kotlinx.coroutines.launch

class MyListViewModel(application: Application) : AndroidViewModel(application) {

    private val dao = MainDB.getDB(application).resultDao()
    private val repo = ResultRepo(dao)

    val cachedResults = repo.allResults

    fun delete(result: CachedResult) = viewModelScope.launch{
        repo.delete(result.id)
    }
}