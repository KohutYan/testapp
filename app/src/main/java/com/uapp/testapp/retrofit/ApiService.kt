package com.uapp.testapp.retrofit

import com.uapp.testapp.response.TenorResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("search")
    suspend fun searchByName(
        @Query("q") q: String? = null,
        @Query("key") key: String? = null,
        @Query("limit") limit: Int = 30
    ): Response<TenorResponse>
}