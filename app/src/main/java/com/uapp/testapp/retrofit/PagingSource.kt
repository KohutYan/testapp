package com.uapp.testapp.retrofit

import androidx.paging.PagingSource
import com.uapp.testapp.response.Result
import retrofit2.HttpException
import java.io.IOException


private const val STARTING_PAGE_INDEX = 1

class TenorPagingSource (
    private val query: String
) : PagingSource<Int, Result>() {

    private val service = RetrofitFactory.makeRetrofitService()

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Result> {
        val position = params.key ?: STARTING_PAGE_INDEX
        return try {
            val response = service.searchByName(q = query)
            val results = response.body()!!.results
            LoadResult.Page(
                data = results,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (results.isEmpty()) null else position + 1
            )
        } catch (ex: IOException){
            return LoadResult.Error(ex)
        } catch (ex: HttpException){
            return LoadResult.Error(ex)
        }
    }

}